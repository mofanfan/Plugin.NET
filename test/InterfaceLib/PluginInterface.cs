﻿using System;

namespace InterfaceLib
{
    /// <summary>
    /// 插件接口
    /// </summary>
    public abstract class PluginInterface
    {
        /// <summary>
        /// 插件名称
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// 插件版本
        /// </summary>
        public abstract Version Version { get; }

        /// <summary>
        /// 插件描述
        /// </summary>
        public virtual string Description { get; }

        /// <summary>
        /// 加载插件后调用，使用了 abstract 修饰，所以必须实现
        /// </summary>
        public abstract void Load();

        /// <summary>
        /// 第一个功能方法，使用了 virtual 修饰，所以可选实现
        /// </summary>
        /// <returns></returns>
        public virtual string Method1()
        {
            return null;
        }

        /// <summary>
        /// 第二个功能方法，使用了 virtual 修饰，所以可选实现
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public virtual string Method2(string from)
        {
            return from;
        }
    }
}
